from flask import Flask
from redis import Redis
import subprocess 

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')
def hello():
    count = redis.incr('hits')
    return ('I\'m {}. I have been seen {} times.\n').format(ip,count)

if __name__ == "__main__":
    cmd = "ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'"
    result = subprocess.run(cmd, shell=True, universal_newlines=True, capture_output=True, check=True)
    ip = result.stdout[:-1]
    app.run(host="0.0.0.0", port=8000, debug=True)
