#/bin/bash

workdir=$(pwd)

docker service create --name registry --publish 5000:5000 registry:2
cd $workdir/stack_python
docker-compose build
docker-compose push

cd $workdir/stack_normal
docker-compose build
