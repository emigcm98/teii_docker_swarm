#/bin/bash

workdir=$(pwd)

# python stack
cd $workdir/stack_python
docker stack deploy --compose-file docker-compose.yml stack_python

# normal stack
cd $workdir/stack_normal
docker stack deploy --compose-file docker-compose.yml stack_normal
